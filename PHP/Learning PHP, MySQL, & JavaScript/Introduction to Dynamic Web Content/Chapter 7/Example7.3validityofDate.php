<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example7.3</title>
  </head>
  <body>
      <?php
        $month = 9;      // September 
        $day   = 31;     // 31st
        $year  = 2018;   // 2018

        if (checkdate($month, $day, $year)) echo "Date is valid";
        else echo "Date is invalid"; 
      ?>
  </body>
</html>