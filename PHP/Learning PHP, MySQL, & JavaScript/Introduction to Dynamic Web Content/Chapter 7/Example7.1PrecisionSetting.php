<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example7.1</title>
  </head>
  <body>
      <?php
        echo "<pre>"; // Enables viewing of the spaces

        //Pad to 15 spaces
        printf("The result is $%15f\n", 123.42 / 12);

        //Pad 15 spaces, fill with zeros 
        printf("The result is $%015f\n", 123.42 / 12);

        //Pad 15 spaces, 2 decimal places precision
        printf("The result is %15.2f\n", 123.42 / 12);

        //Pad 15 spaces, 2 decimal places precision fill with zeros
        printf("The result is $%015.2f\n", 123.42 / 12);

        //Pad to 15 spaces, 2 decimal places precision, fill with # symbol
        printf("The result is $%'#15.2f\n", 123.42 / 12);
      ?>
  </body>
</html>