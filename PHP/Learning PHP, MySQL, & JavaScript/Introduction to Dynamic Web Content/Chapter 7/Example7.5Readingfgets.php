<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example7.4</title>
  </head>
  <body>
      <?php
         $fh = fopen("testfile.txt", 'r') or
          die("File does not exist or you lack permission to open it");
         
         $line = fgets($fh);
         fclose($fh);
         echo $line; 
      ?>
  </body>
</html>