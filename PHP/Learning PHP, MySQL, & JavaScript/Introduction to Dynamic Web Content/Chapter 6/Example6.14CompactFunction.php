<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example6.14</title>
  </head>
  <body>
      <?php
             $fname       = "Doctor";
             $sname       = "Who";
             $planet      = "Gallifrey";
             $system      = "Gridlock";
             $constellation = "Kasterborous";
             
             $contact = compact('fname', 'sname', 'planet', 'system', 'constellation');

             print_r($contact);
      ?>
  </body>
</html>