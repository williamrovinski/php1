<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example6.8</title>
  </head>
  <body>
      <?php
           $paper = array('copier' => "Copier & Multipurpose",
                          'inkjet' => "Inkjet Printer",
                          'laser' => "Laser Printer",
                          'photo' => "Photographic Paper");
            while (list($item, $description) = each($paper))
                echo "$item: $description<br>";
      ?>
  </body>
</html>