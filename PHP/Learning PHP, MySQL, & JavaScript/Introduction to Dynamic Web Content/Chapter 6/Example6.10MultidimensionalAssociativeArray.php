<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example6.10</title>
  </head>
  <body>
      <?php
           $products = array(

           'paper' => array(
               'copier' => "Copier & Multipurpose",
               'inkjet' => "Inkjet Printer",
               'laser' => "Laser Printer",
               'photo' => "Photographic Paper"),

            'pens' => array(
                'ball'   => "Ball Point",
                'hilite' => "Highlighters",
                'marker' => "Markers"),

            'misc' => array(
                'tape' => "Sticky Tape",
                'glue' => "Adhesives",
                'clips' => "Paperclips"
            )
            );

            echo "<pre>";

            foreach($products as $section => $items)
                foreach($items as $key => $value)
                    echo "$section:\t$key\t($value)<br>";

            echo "</pre>";
      ?>
  </body>
</html>