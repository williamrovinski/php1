<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example6.7</title>
  </head>
  <body>
      <?php
           $paper = array('copier' => "Copier & Multipurpose",
                          'inkjet' => "Inkjet Printer",
                          'laser' => "Laser Printer",
                          'photo' => "Photographic Paper");
            foreach($paper as $item => $description)
                echo "$item: $description<br>";
      ?>
  </body>
</html>