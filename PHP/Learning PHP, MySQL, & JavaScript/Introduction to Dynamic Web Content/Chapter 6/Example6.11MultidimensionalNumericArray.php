<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example6.10</title>
  </head>
  <body>
      <?php
           $chessboard = array(
               array('r', 'n', 'b', 'q', 'k', 'b', 'n', 'r'),
               array('p', 'p', 'p', 'p', 'p', 'p', 'p', 'p'),
               array(' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
               array(' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
               array(' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
               array(' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
               array('P', 'P', 'P', 'P', 'P', 'P', 'P', 'P'),
               array('R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R'),
           );

           echo "<pre>";
           foreach($chessboard as $row)
           {
               foreach ($row as $piece)
                  echo "$piece ";

                echo "<br>";
           }
           
           echo "</pre>";
           echo $chessboard[6][4];
           echo $chessboard[7][5];
      ?>
  </body>
</html>