<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Example6.5</title>
  </head>
  <body>
      <?php
           $p1 = array("Copier", "Inkjet", "Laser", "Photo");

           echo "p1 element: " . $p1[2] . "<br>";

           $p2 = array('copier' => "Copier & Multipurpose",
                       'inkjet' => "Inkjet Printer",
                       'laser' => "Laser Printer",
                       'photo' => "Photographic Paper");
            
            echo "p2 element: " . $p2['inkjet'] . "<br>";
      ?>
  </body>
</html>