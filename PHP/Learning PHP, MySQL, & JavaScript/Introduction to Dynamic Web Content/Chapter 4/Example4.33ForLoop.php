<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example4.33</title>
</head>
<body>
    <?php
        ##For loop similiar to the do while but does the same thing. 
        for($count = 1 ; $count <= 12 ; ++$count) 
        {
            echo "$count times 12 is " . $count * 12 . "<br>";
            echo "<br>";
        } ## While I don't need the curly brackets it helps for structuring the way it looks in case I wanted to close it with an elseif or else.
    ?>    
</body>
</html>