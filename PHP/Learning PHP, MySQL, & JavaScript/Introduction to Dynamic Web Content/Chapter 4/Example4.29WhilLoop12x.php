<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example4.29</title>
</head>
<body>
    <?php
        $count = 1;

        while ($count <= 30)
        {
            echo "$count times 30 is " . $count * 30 . "<br>";
            ++$count;      ## Similiar to JavaScript ++ gets the number and adds it by one.  
        }
        ##Or you could write it
        echo "<br>";
        $number = 5;

        while (++$number <= 30)
        {
            echo "$number times 30 is " . $number * 30 . "<br>";
            ##++$count; Now you don't have to call it here.  
        }
    ?>    
</body>
</html>