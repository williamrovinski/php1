<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example4.2</title>
</head>
<body>
    <?php
        $myname = "Brian";
        $myage = 37;
        echo "a: " . 73 . "<br>"; // Numeric literal
        echo "b: " . "Hello" . "<br>"; // String literal
        echo "c: " . FALSE . "<br>"; // Constant literal
        echo "d: " . $myname . "<br>"; // String variable
        echo "e: " . $myage . "<br>"; // Numeric variable
    ?>    
</body>
</html>