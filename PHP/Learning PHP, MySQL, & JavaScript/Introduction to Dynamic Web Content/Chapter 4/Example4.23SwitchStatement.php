<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example4.23</title>
</head>
<body>
    <?php
    ## I use switch to go through a if stamement type construction and cycles through. I then have a break method.
    ##break command tells PHP that once the condition is met, it'll then execute and stop cycling through the construction. 
    $page = "Login"; 
          switch ($page)
          {
              case "Home";
              echo "You selected Home";
              break;
              case "About";
              echo "You selected About";
              break;
              case "News";
              echo "You selected News";
              break;
              case "Login";
              echo "You selected Login";
              break;
              case "Links";
              echo "You selected Links";
              break;
          }
    ?>    
</body>
</html>