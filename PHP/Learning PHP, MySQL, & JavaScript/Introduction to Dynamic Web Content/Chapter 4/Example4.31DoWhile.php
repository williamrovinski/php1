<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example4.29</title>
</head>
<body>
    <?php
      $count = 2;

      do 
          echo "$count times 12 is " . $count * 12 . "<br>";
      while (++$count <= 12); 
    ?>    
</body>
</html>