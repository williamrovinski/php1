<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Example12.1</title>
</head>
<body>
  <?php
    if (isset($_SERVER['PHP_AUTH_USER']) &&
        isset($_SERVER['PHP_AUTH_PW']))
    {
        echo "Welcome User: " . $_SERVER['PHP_AUTH_USER'] .
             " Password: " . $_SERVER['PHP_AUTH_PW'];
    }
    else
    {
        header('WWW-Authenticate: Basic realm="Restricted Section"');
        header('HTTP/1.0 401 Unauthorized');
        die("Please enter your username and password");
    }
    ?>
</body>
</html>