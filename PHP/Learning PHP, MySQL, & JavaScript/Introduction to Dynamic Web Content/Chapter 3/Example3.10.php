<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example3.10</title>
</head>
<body>
    <?php

    $number = 12345 * 67890;
    echo substr($number, 3, 1);  ##function substr, which asks for one character to be returned from $number, starting at the fourth position.

    ?>
    
</body>
</html>