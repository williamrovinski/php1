<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example3.14</title>
</head>
<body>
    <?php
        $temp = "The date is ";
        echo longdate(time());
        
        function longdate($timestamp) 
        {
            return $temp . date("l F jS Y", $timestamp);
        }
    ?>    
</body>
</html>