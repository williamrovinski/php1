<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example3.9</title>
</head>
<body>
    <?php 
       $author = "Scott Adams"; 
       
       echo <<<_END
       Normal people believe that if it ain't broke don't fix it. Engineers believe that if it ain't broke it doesn't have enough features yet.

       - $author.
_END;

    ?>
    
</body>
</html>