<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example3.12</title>
</head>
<body>
    <?php
        function longdate($timestamp) {
            $temp = date("l F jS Y", $timestamp);
            return "The date is $temp";
        }
        echo longdate(time());
    ?>    
</body>
</html>