<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example3.16</title>
</head>
<body>
    <?php
     $temp = "The date is ";
     echo longdate($temp, time());

     function longdate($text, $timestamp) 
     {
         return $text . date("l F  jS Y" , $timestamp);
     }
    ?>    
</body>
</html>