<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example3.19</title>
</head>
<body>
    <?php
     static $int = 0;        // Will be allowed
     static $int = 1+2;      // Disallowed
     static $int = sqrt(144); // Disallowed
    ?>    
</body>
</html>