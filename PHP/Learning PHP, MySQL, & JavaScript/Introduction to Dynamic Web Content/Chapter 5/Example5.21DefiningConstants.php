<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example5.20</title>
</head>
<body>
    <?php
        class Translate
        {
            const ENGLISH = 0;
            const SPANISH = 1;
            const FRENCH = 2;
            const GERMAN = 3;
            //....
            static function lookup()
            {
                echo self::SPANISH;
            }
        }

    ?>    
</body>
</html>