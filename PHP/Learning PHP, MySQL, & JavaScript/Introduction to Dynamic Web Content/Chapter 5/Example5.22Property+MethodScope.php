<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example5.22</title>
</head>
<body>
    <?php
        class Example
        {
            var $name = "Michael";  // Same as public but deprecated
            public $age = 23;       // Public property
            protected $usercount;   // Protected property

            private function admin() // Private method
            {
                //Admin code goes here
            }
        }

    ?>    
</body>
</html>