<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example4.12</title>
</head>
<body>
    <?php
         $names = fix_names("WILLIAM", "henry", "gatES");
         echo $names[0] . " " . $names[1] . " " . $names[2];

         function fix_names($n1, $n2, $n3)
         {
             $n1 = ucfirst(strtolower($n1)); 
             $n2 = ucfirst(strtolower($n2)); 
             $n3 = ucfirst(strtolower($n3));
             return array($n1, $n2, $n3);  
         }
    ?>    
</body>
</html>