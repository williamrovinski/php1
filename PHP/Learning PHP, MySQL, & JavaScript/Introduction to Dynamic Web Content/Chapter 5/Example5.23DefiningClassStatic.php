<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example5.20</title>
</head>
<body>
    <?php
    $temp = new Test();

    echo "TextA: " . Test::$static_property . "<br>";
    echo "TextB: " . $temp->get_sp() . "<br>";
    echo "TextC: " . $temp->static_property . "<br>";

    class Test
    {
        static $static_property = "I'm stattic";
        function get_sp()
        {
            return self::$static_property = "I'm static"; 
        }
    }

    ?>    
</body>
</html>