<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example4.12</title>
</head>
<body>
    <?php
        echo strrev(" .dlrow olleh"); //strrev means reverse string
        echo str_repeat("Hip ", 2);   // Repeat string
        echo strtoupper("hooray!");   //strtoupper converts all characters to uppercase.
    ?>    
</body>
</html>