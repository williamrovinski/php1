<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>String Functions</title>
</head>
<body>

<?php 

$string = "Hello student do you like the class."; 

echo strlen($string);     ## strlen is a built in function, returns length of string.
echo "<br>";
echo strtoupper($string);        ## strtoupper, converts the parameter, which is a string to upper case. 
echo "<br>";
echo strtolower($string);        ## strtolower, 
?>
</body>
</html>