<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Math Functions</title>
</head>
<body>

<?php 

echo pow(2,7);   ##pow stands for power as in multiplying an object or integer by itself. 
echo "<br>";
echo rand();
echo "<br>";
echo rand(5, 60);
echo "<br>";
echo sqrt(1000);     ##sqrt stands for the square root of something, that something is the parameter you set.
echo "<br>";
echo ceil(4.6);      ## rounds numbers to the nearest greatest integer.
echo "<br>"; 
echo floor(4.6);    ## rounds numbers down to the nearest integer.
echo "<br>";
echo round(4.5);     ## rounds up to the nearest integer. 


?>
</body>
</html>