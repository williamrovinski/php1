<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Functions</title>
</head>
<body>
    <?php 

      function addNumbers($number1, $number2) {
          $sum = $number1 + $number2; 
          return $sum;
      }
      $result = addNumbers(6,10);
      $result = addNumbers(90, $result);
      echo $result;
    ?>
</body>
</html>
