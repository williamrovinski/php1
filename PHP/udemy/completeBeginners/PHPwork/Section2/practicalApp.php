<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Associative Arrays</title>
</head>
<body>

    <?php 
    $number1 = 10;
    $number2 = 20;
    echo $number1 + $number2; 
    
    $names = array('Frank', 'Will', 'Tom');
    $names = array("first_name" => 'Frank', "second_name" => 'Tom');
    echo "<br>";
    print_r($names); 
    echo "<br>";
    echo $names['first_name']. " " .$names['second_name'];
    echo "<br>";
    $number1 = 10;
    $number2 = 20;

    echo $number1 + $number2 . "<br>";
    
    $lists = array(23,24,543,234,7654,3456);
    $lists_assoc = array("number" => 10);
    echo $lists[0] . " " . "<br>";
    echo $lists_assoc['number'] . "<br";

    ?>
</body>
</html>