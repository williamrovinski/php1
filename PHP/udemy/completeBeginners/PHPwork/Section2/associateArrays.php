<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Associative Arrays</title>
</head>
<body>

    <?php 
    
    $number = array(10, 20, 49);
    $names = array("juanita", "maria", "jose");
    echo $number[2] . "<br>";
    print_r($number);
    echo "<br>";

    $names = array("first_name" => 'Edwin', "second_name" => 'Juanita');
    
    print_r($names); 
    echo "<br>";
    echo $names['first_name']. " " .$names['second_name'];
  
    ?>
</body>
</html>