<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Variables</title>
</head>
<body>

	<div id="navigation">

        <?php 
        // This is how you write variables
        $name = "Edwin";
        $number = 100; 
        $numberList = 200;
        $number_List = 300;
        $name2 = "<h1> Hello</h1>"; 
        
        echo $name; 
        echo "<br>"; 
        echo $number;
        echo "<br>";  
        echo $number_List;
        echo "<br>"; 
        echo $numberList;
        echo "<br>";  
        echo $name. " " .$number_List; //To concatenate make sure you use a dot and then a quotation to get a space.
        echo $name2; 
        ?>
</body>
</html>
