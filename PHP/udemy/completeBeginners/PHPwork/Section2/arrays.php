<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Arrays</title>
</head>
<body>

        <?php 
        //Creating Arrays
        
        //First method of array
        $numberList0 = array(23,64,267,478,267,8765,345, '5345','345', '<h1>Hello</h1>');
        //second method of array
        $numberList1 = [2,34,2,4,5,6,7];

        echo $numberList0[2]; //This displays the 3 item, value 2 of the array.
        echo "<br>";
        print_r($numberList0); //This display the entirety of the array

        ?>
</body>
</html>