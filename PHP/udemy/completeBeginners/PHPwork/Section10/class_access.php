<?php

class Car {

    public $wheels = 4;   //unsecure
    protected $hood = 1;  //semi secure
    private $engine = 1;  //very secure
    var $doors = 4;       //plain local vareiable

    function MoveWheels(){

       $this->wheels = 10;
    }

    function showProperty() {/*In PHP, protected variables within a class or anywhere can only be accessed
                            through a function/method within the class. In order to show protected variable
                            you'd have to echo said method/function.*/ 

        echo $this->hood;
        echo "<br>";
        echo $this->engine; /*Special note to private variables, they can only be used is a specified 
                              function/method, if an identical one existed in class Semi, it would fail 
                              to load the private variable must be call in the specific class 
                              function/method it was assigned to.*/
    }
}

$bmw = new Car();
echo $bmw->wheels;
print "<br>";
echo $bmw->showProperty() . "<br>";
$semi = new Semi();
echo $semi->showProperty();
print "<br>";
$truck = new Truck(); 
echo $truck->ShowLocalVariable();

class Semi extends Car{

}

class Truck extends Car {  //remember "extends" allows you to add other class attributes to your class 
                           //you're trying to specify. In this case Truck picks up Cars class attributes 
    function ShowLocalVariable(){
        echo $this->doors;
    }
} 
?>