<?php
class Dog {
    var $eyeColor = 'amber';
    var $nose = 'long';
    var $furColor = 'Heathergray';

    function showAll() {
        echo $this->eyeColor;
        echo "<br>";
        echo $this->nose;
        echo "<br>";
        echo $this->furColor;
    }
}

$wolf = new Dog();
$wolf->showAll();

?>