<?php

class Car {

    static $wheels = 4; //static
    var $hood = 1;  
    var $engine = 1;  
    var $doors = 4;       

    function MoveWheels(){

       Car::$wheels = 10; // this is how a static variable is called in the function. 
    }
}

$bmw = new Car();
//$bmw->wheels;
Car::MoveWheels(); 
echo Car::$wheels; /*static variable are tied only to the specific class, also they are called with a ::
                     in order to echo the function's variable you first must call the function/method
                     then you must echo it latter like this.*/
?>