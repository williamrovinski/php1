<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>If Statement</title>
</head>
<body>
    <?php

    if(3 > 10){
        echo "3 is greater than 10.";
    } elseif(4 > 5) {
        echo " of course 4 is less than 5"; 
    } 
    else {
        echo "3 is less than 10";
    }

    ?>
</body>
</html>