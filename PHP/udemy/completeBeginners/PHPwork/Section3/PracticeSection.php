<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Practice Section</title>
</head>
<body>

    <?php
    if( 4 > 7) {
        echo "I love PHP";
    } 
    elseif( 5 == 90) {
        echo " I love Anime";
    }
    else {
        echo "I love PHP <br>";
    }

    // For loops require 3 parameters in the paranthesis, and the variable has to be defined in the parameter not outside. 
    for($number = 0; $number < 11; $number++) {
        echo  $number . "<br>";
    }

    $widgets = 2;
    switch(2) {
        case 1;
        echo "It is 1";
        break;
        case 3;
        echo "It is 3";
        break;
        case 4;
        echo "It is 4";
        break;
        case 5;
        echo "It is 5";
        break;
        case 2;
        echo "It is 2";
        break;
        
        
    }
    ?>
</body>
</html>