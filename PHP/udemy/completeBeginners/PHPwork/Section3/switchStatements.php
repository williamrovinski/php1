<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Switch Statements</title>
</head>
<body>

    <?php

    $number = 4;

    if($number < 10) {
        echo "this"; 
    }
    elseif($number < 20) {
        echo "This is also true";
    }
    elseif($number < 30) {
        echo "True statement";
    }
    else{ ##else statements that end the if else chain require no parameters. 
        echo "print nothing";
    }

    $number1 = 34;

    switch($number1){
        case 34:
        echo "<br>is it 34";
        break; ## break ends the comments once it finds what it needs, it ends the printing once it finds its answer no more no less.
        case 37:
        echo "<br>is it 37";
        break;
        case 35:
        echo "<br>is it 35";
        break;
        case 24:
        echo "<br>is it 24";
        break;
        
        default: 
        echo "we could not find anything.";
        break;
    }
    ?>
</body>
</html>