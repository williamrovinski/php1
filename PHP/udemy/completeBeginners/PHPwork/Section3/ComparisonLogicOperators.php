<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Comparison and Logical Operators</title>
</head>
<body>
    <!--<h2>Comparison Operators</h2>
    <pre>
        equal ==
        identical ===
        compare > < >= <= <>
        not equal !=
        not identical !==
        
        <h2>Logical Operators</h2>
        <pre>
        and &&
        or ||
        not !
        </pre>

    </pre>-->
    <?php
    if (4 != 6){
       echo "it is true";
    }

    if (4 === "4" && 3 > 1){
        echo "This looks good.";
    } elseif (5 > 2 || 4 === 4){
        echo "<br> This looks even better.";
    }

    ?>
</body>
</html>