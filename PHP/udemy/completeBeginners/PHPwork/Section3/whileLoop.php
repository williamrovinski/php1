<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>While Loop</title>
</head>
<body>

    <?php
    
    $counter = 0; 

    while($counter < 10){
        echo "hello students";
        // Method 1, $counter = $counter + 1; ## this increments counter by 1 each time. 
        /* Method 2 */ $counter++; //This also increments it by 1. 
    }

    
    ?>
</body>
</html>