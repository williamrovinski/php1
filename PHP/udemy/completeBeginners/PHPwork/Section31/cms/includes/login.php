<?php include "db.php"; ?>

<?php session_start(); ?>

<?php

if(isset($_POST['login'])) {
    $username1 = $_POST['user_name'];  //The thing posted must match the database, the 
                                           //variable name itself can be anything. 
    $password1 = $_POST['user_password']; //The thing posted must match the database, the 
                                            //variable name itself can be anything.

$username1 = mysqli_real_escape_string($connection, $username1);
$password1 = mysqli_real_escape_string($connection, $password1); 

$query = "SELECT * FROM users WHERE user_name = '{$username1}' ";
$select_user_query = mysqli_query($connection, $query);

if(!$select_user_query) {

    die("QUERY FAILED". mysqli_error($connection));
} 

while($row = mysqli_fetch_array($select_user_query)) {
    $db_id = $row['user_id'];
    $db_username = $row['user_name'];
    $db_password = $row['user_password'];
    $db_firstname = $row['user_firstname'];
    $db_lastname = $row['user_lastname'];
    $db_role = $row['user_role'];
    
}

/*Here's how to test if your encryption string is 22 characters long.
$password = crypt($password, $db_password);
$hash_format = "2y$10&"; 

$salt = "iusesomecrazystrings22";
echo strlen($salt);
crypt($password, "iusesomecrazystrings22");*/


    if($username1 ===  $db_username && $password1 === $db_password) {
    //if(password_verify($password1, $db_password) ) { // a shorter construction, using password_verify construction 
        echo $_SESSION['username'] = $db_username;    // These SESSION[] values don't have to 
                                                      // equal anything prior in the array brackets
                                                      //They are new entities
        echo $_SESSION['firstname'] = $db_firstname;
        echo $_SESSION['lastname'] = $db_lastname;
        echo $_SESSION['user_role'] = $db_role;      // But if you did want to make it equal 
                                                     // to something prior, the session will retireve it. 
        
        header("Location: ../admin");
        
     } else {
        header("Location: ../index.php");  
    }
}

?>