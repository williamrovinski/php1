<!-- Blog Sidebar Widgets Column -->

<div class="col-md-4">


                <!-- Blog Search Well -->
                <div class="well">

                <?php

                    $query = "SELECT * FROM categories LIMIT 4";
                    $select_categories_sidebar = mysqli_query($connection,$query);

                ?> 
                              

                <!--Blog Search-->
                <h4>Blog Search</h4>
                    <form action="search.php" method="post">
                    <div class="input-group">
                        <input name="user_name" type="text" class="form-control">
                                <span class="input-group-btn">
                        
                        <button name="submit" button class="btn btn-default" type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    </form><!--search form-->
                    <!-- /.input-group -->
                    </div>

                    <!--Login-->
                <h4>Login</h4>
                    <form action="includes/login.php" method="post">
                    <div class="input-group">
                        <input name="user_name" type="text" class="form-control" placeholder="Enter Username">
                    
                    </div>
                    <input name="user_password" type="password" class="form-control" placeholder="Enter Password">
                    
                    </div>
                    </form><!--search form-->
                    <!-- /.input-group -->
                </div>
                
        
                

                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Blog Categories</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="list-unstyled">

                            <?php
                                while($row = mysqli_fetch_assoc($select_categories_sidebar)) {
                                    $cat_title = $row['cat_title'];
                                    $cat_id = $row['cat_id'];
                                    
                                echo "<li><a href='category.php?category=$cat_id'>{$cat_title}</a></li>";
                                }
                            ?>

                            </ul>
                        </div>
                        
                        
                    </div>
                    <!-- /.row -->
                </div>

                <!-- Side Widget Well -->
                <?php include "includes/widget.php"; ?>