<?php # Script 18.5 -index.php
//This is the main page for the site.

// Include the configuration file: 
require('includes/config.inc.php');

// Set the page title and include the HTML header:
$page_title = 'Welcome to this Site!';
include('includes/header.php');

// Welcome the user (by name if they are logged in):

    echo '<h1>Welcome';
    if (isset($_SESSION['first_name'])) {
        echo ", {$_SESSION['first_name']}";
    }
    echo '!</h1>';
?>

    <p><span class="indexP">New Users please register to access the rest of this site. Instructions will be provided in your email. <br>
    For already existing users, please use your email and password to login. <br>
    You'll find some fun games here. Check out your friend's scores so you know who to beat in the future. </span>
    </p>

<?php include('includes/footer.php'); ?>