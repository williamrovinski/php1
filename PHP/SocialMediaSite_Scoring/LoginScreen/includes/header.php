<?php # Script 18.1 - header.html
// This page begins the HTML header for the site.


// Start output buffering: 
ob_start();

// Check for a $page_title value:
if (!isset($page_title)) {
    $page_title = 'User Registration'; 
}
?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title><?php echo $page_title;
        ?></title>
    <link rel="stylesheet" type="text/css" href="includes/layout.css">
    
</head>
<body>
<div id="Header">Gaming Wonderland</div>
<div id="content">
<!--End of Header -->


